-- @block create employees table
-- @conn PGSQL
/*Создать таблицу с основной информацией о сотрудниках: 
ФИО, дата рождения, дата начала работы, должность, уровень сотрудника (jun, middle, senior, lead), уровень зарплаты, 
идентификатор отдела, наличие/отсутствие прав(True/False). 
При этом в таблице обязательно должен быть уникальный номер для каждого сотрудника.
*/


CREATE type grade_type as enum('junior', 'middle', 'senior', 'lead');
CREATE table  if not exists employees (
id INT generated always as identity primary key,
FIO VARCHAR(255) Not Null,
birthdate DATE Not Null,
begindate DATE Not Null,
post VARCHAR(20) ,
grade grade_type Not Null,
salary INT,
department_id INT,
driver_licence Boolean
) 
/*2. Для будущих отчетов аналитики попросили вас создать еще одну таблицу с информацией по отделам 
– в таблице должен быть идентификатор для каждого отдела, 
название отдела (например. Бухгалтерский или IT отдел), ФИО руководителя и количество сотрудников.*/
create table if not exists departments (
id INT generated always as identity primary key,
name VARCHAR(255) Not Null,
manager VARCHAR(255) Not Null,
number_of_employees INT
)
 /*  3. На кону конец года и необходимо выплачивать сотрудникам премию. Премия будет выплачиваться по совокупным 
 оценкам, которые сотрудники получают в каждом квартале года. 
 Создайте таблицу, в которой для каждого сотрудника будут его оценки за каждый квартал.
 Диапазон оценок от A – самая высокая, до E – самая низкая.*/
 create type estimation as enum('A', 'B', 'C', 'D', 'E');
    create table if not exists estimations (
    id INT generated always as identity primary key,
    employee_id INT Not Null,
    grade1 estimation Not Null,
    grade2 estimation Not Null,
    grade3 estimation Not Null,
    grade4 estimation Not Null,
    FOREIGN KEY (employee_id) REFERENCES employees(id)
    )   
/*4. После того, как вы создали таблицы, необходимо заполнить их данными.
Создайте скрипт, который будет заполнять таблицы данными.*/
INSERT INTO departments (name, manager, number_of_employees) VALUES ('Бухгалтерский', 'Иванов И.И.', 5);
INSERT INTO departments (name, manager, number_of_employees) VALUES ('IT', 'Петров П.П.', 10);
INSERT INTO departments (name, manager, number_of_employees) VALUES ('Интеллектуального анализа данных', 'Сидоров С.С.', 15);

INSERT INTO estimations (employee_id, grade1, grade2, grade3, grade4) VALUES (1, 'A', 'A', 'A', 'A');
INSERT INTO estimations (employee_id, grade1, grade2, grade3, grade4) VALUES (2, 'B', 'B', 'B', 'B');
INSERT INTO estimations (employee_id, grade1, grade2, grade3, grade4) VALUES (3, 'C', 'C', 'C', 'C');
INSERT INTO estimations (employee_id, grade1, grade2, grade3, grade4) VALUES (4, 'D', 'D', 'D', 'D');
INSERT INTO estimations (employee_id, grade1, grade2, grade3, grade4) VALUES (5, 'E', 'E', 'E', 'E');

 /*5. Создайте скрипт, который будет выводить   Уникальный номер сотрудника, его ФИО и стаж работы – для всех сотрудников компании */
select id, FIO, (current_date - begindate) as experience from employees;
/*6. Создайте скрипт, который будет выводить    
Уникальный номер сотрудника, его ФИО и стаж работы – только первых 3-х сотрудников */
select id, FIO, (current_date - begindate) as experience from employees limit 3;

/*7. Создайте скрипт, который будет выводить
# Уникальный номер сотрудников - водителей */
select id from employees where driver_licence = true;

/*8. Создайте скрипт, который будет выводить
# Выведите номера сотрудников, которые хотя бы за 1 квартал получили оценку D или E */
SELECt id from estimations where grade1 = 'D' or grade1 = 'E' or grade2 = 'D' or grade2 = 'E' or grade3 = 'D' or grade3 = 'E' or grade4 = 'D' or grade4 = 'E';

/*9. Создайте скрипт, который будет выводить    
Выведите самую высокую зарплату в компании.*/
SELECT MAX(salary) from employees;   

/*10. Создайте скрипт, который будет выводить
Выведите название самого крупного отдела в компании.*/
SELECT MAX(number_of_employees) from departments;

/*11. Создайте скрипт, который будет выводить
Выведите номера сотрудников от самых опытных до вновь прибывших*/
SELECT id, FIO, (current_date - begindate) as experience from employees order by experience desc;

/*12. Создайте скрипт, который будет выводить
Рассчитайте среднюю зарплату для каждого уровня сотрудников */
SELECT AVG(salary) from employees GROUP BY grade;
