-- @block create employees table
-- @conn PGSQL

/*13. Создайте скрипт, который будет выводить
вывести именно фамилию сотрудника с самой высокой зарплатой.*/
SELECT FIO from employees where salary = (SELECT MAX(salary) from employees);

/*14. Создайте скрипт, который будет выводить
фамилии сотрудников в алфавитном порядке*/
SELECT FIO from employees order by FIO;

/*15. Создайте скрипт, который будет выводить
средний стаж для каждого уровня сотрудников*/
SELECT AVG(current_date - begindate) from employees GROUP BY grade;

/*16. Создайте скрипт, который будет выводить
фамилию сотрудника и название отдела, в котором он работает*/
SELECT FIO, departments.name from employees, departments where employees.department_id = departments.id;

/*17. Создайте скрипт, который будет выводить
фамилию сотрудника и название отдела, в котором он работает, если он работает в отделе с количеством сотрудников больше 10*/
SELECT FIO, departments.name from employees, departments where employees.department_id = departments.id and departments.number_of_employees > 10;

/*18. Создайте скрипт, который будет выводить
название отдела и фамилию сотрудника с самой высокой зарплатой в данном отделе
 и саму зарплату также*/
 select departments.name, employees.FIO, employees.salary from employees, departments where employees.department_id = departments.id and employees.salary = (select MAX(salary) from employees where employees.department_id = departments.id);


/*19. Создайте скрипт, который будет выводить
